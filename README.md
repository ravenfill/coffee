<h1 align="center">Welcome to Coffee website 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.0.7-blue.svg?cacheSeconds=2592000" />
</p>

> A test of coffee website with docker containering

### 🏠 [Homepage](https://gitlab.com/ravenfill/coffee.git)

## How To Install

Clone git repository and build image from source 
```sh
git clone https://gitlab.com/ravenfill/coffee.git
cd coffee
docker build .
```

Either way you can just grab an image from Docker Hub
```sh
docker pull ravenfill/coffee-time:latest
```

Build with .sh script
```sh
chmod +x build.sh
./build.sh
```

## Author

👤 **Raven Fill**


***
website made by [@rominamartinlib](https://twitter.com/rominamartinlib?s=09)
***

_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
